package me.teknight.elytra_plugin;

import org.bukkit.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.*;


import java.text.DecimalFormat;

/**
 * Created by teknight on 2/27/16.
 */
public class elytra_movement implements Listener{

    final static String win_message  = "§aYay you completed the map! GZ§r";
    final static String win_message_multi  = "§aYay you completed the multimap! GZ§r";
    final static String lose_message = "§eOh you didn't complete... how sad :-(§r";

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event)
    {
        int player_playing_idx = plugin_memory.player_playing(event.getPlayer());
        if (player_playing_idx == -1) return;

        if (event.getPlayer().getFallDistance() != 0 && !plugin_memory.current_playing_players.get(player_playing_idx).started && (System.currentTimeMillis() - plugin_memory.current_playing_players.get(player_playing_idx).join_time) > 1500)
        {
            plugin_memory.current_playing_players.get(player_playing_idx).started = true;
        }

        if (plugin_memory.maps.get(plugin_memory.current_playing_players.get(player_playing_idx).map_idx).checkpoints.size() <= plugin_memory.current_playing_players.get(player_playing_idx).current_checkpoint) return;
        Location plr_loc = event.getPlayer().getLocation();
        if (plr_loc.distance(plugin_memory.maps.get(plugin_memory.current_playing_players.get(player_playing_idx).map_idx).checkpoints.get(plugin_memory.current_playing_players.get(player_playing_idx).current_checkpoint).get(0).loc) < 12) {
            for (loc_points location : plugin_memory.maps.get(plugin_memory.current_playing_players.get(player_playing_idx).map_idx).checkpoints.get(plugin_memory.current_playing_players.get(player_playing_idx).current_checkpoint)) {
                if (plr_loc.distance(location.loc) < ((event.getPlayer().getVelocity().length()*1.0 > 1.0) ? event.getPlayer().getVelocity().length()*1.0 : 1.0))
                {
                    if (plugin_memory.current_playing_players.get(player_playing_idx).current_checkpoint == 0 && !plugin_memory.current_playing_players.get(player_playing_idx).mm_started)
                    {
                        plugin_memory.current_playing_players.get(player_playing_idx).start_time = System.currentTimeMillis();
                    }
                    if (location.loc_type == plugin_memory.location_types.BLOCK_BOOST) event.getPlayer().setVelocity(event.getPlayer().getVelocity().add(event.getPlayer().getLocation().getDirection().multiply(event.getPlayer().getVelocity().length() * 1.0)));
                    if (location.loc_type == plugin_memory.location_types.BLOCK_SLOW) event.getPlayer().setVelocity(event.getPlayer().getVelocity().multiply(0.5));
                    event.getPlayer().sendMessage("Went through §a" + ++plugin_memory.current_playing_players.get(player_playing_idx).current_checkpoint + "§r / §a" + plugin_memory.maps.get(plugin_memory.current_playing_players.get(player_playing_idx).map_idx).checkpoints.size() + "§r checkpoints!");
                    if (plugin_memory.current_playing_players.get(player_playing_idx).current_checkpoint == plugin_memory.maps.get(plugin_memory.current_playing_players.get(player_playing_idx).map_idx).checkpoints.size())
                    {
                        if (plugin_memory.current_playing_players.get(player_playing_idx).mm.size() != 0)
                        {
                            plugin_memory.current_playing_players.get(player_playing_idx).mm_started = true;
                            plugin_memory.current_playing_players.get(player_playing_idx).mm.remove(0);
                            if (plugin_memory.current_playing_players.get(player_playing_idx).mm.size() == 0)
                            {
                                DecimalFormat df = new DecimalFormat("0.00");
                                long end_time = System.currentTimeMillis() - plugin_memory.current_playing_players.get(player_playing_idx).start_time;
                                plugin_memory.current_playing_players.get(player_playing_idx).restart = false;
                                plugin_memory.add_to_mm_highscore(plugin_memory.current_playing_players.get(player_playing_idx).mm_idx, end_time, event.getPlayer().getDisplayName());
                                Bukkit.broadcastMessage(event.getPlayer().getDisplayName() + " ended multimap: §b" + plugin_memory.multimaps.get(plugin_memory.current_playing_players.get(player_playing_idx).mm_idx).multimap_name + "§r in time: §a" + df.format((double) (end_time / 1000.0)) + "§r");
                                event.getPlayer().sendMessage("Highscores for this map:");
                                for (int i = 0; i < plugin_memory.multimaps.get(plugin_memory.current_playing_players.get(player_playing_idx).mm_idx).highscore.size(); i++) {
                                    event.getPlayer().sendMessage((i + 1) + ". " + df.format((double) (plugin_memory.multimaps.get(plugin_memory.current_playing_players.get(player_playing_idx).mm_idx).highscore.get(i).time / 1000.0)) + " " + plugin_memory.multimaps.get(plugin_memory.current_playing_players.get(player_playing_idx).mm_idx).highscore.get(i).name);
                                }
                                player_end(player_playing_idx, win_message_multi);
                            }
                            else
                            {
                                plugin_memory.current_playing_players.get(player_playing_idx).map_idx = plugin_memory.current_playing_players.get(player_playing_idx).mm.get(0);
                                player_end(player_playing_idx, "§aYay you completed this level, only §b" + plugin_memory.current_playing_players.get(player_playing_idx).mm.size() + "§a levels back!§r");
                            }
                        }
                        else {
                            DecimalFormat df = new DecimalFormat("0.00");
                            long end_time = System.currentTimeMillis() - plugin_memory.current_playing_players.get(player_playing_idx).start_time;
                            int map_idx = plugin_memory.current_playing_players.get(player_playing_idx).map_idx;
                            plugin_memory.add_to_highscore(plugin_memory.current_playing_players.get(player_playing_idx).map_idx, end_time, event.getPlayer().getDisplayName());

                            Bukkit.broadcastMessage(event.getPlayer().getDisplayName() + " ended map: §b" + plugin_memory.maps.get(map_idx).map_name + "§r in time: §a" + df.format((double) (end_time / 1000.0)) + "§r");
                            player_end(player_playing_idx, win_message);
                            event.getPlayer().sendMessage("Highscores for this map:");
                            for (int i = 0; i < plugin_memory.maps.get(map_idx).highscore.size(); i++) {
                                event.getPlayer().sendMessage((i + 1) + ". " + df.format((double) (plugin_memory.maps.get(map_idx).highscore.get(i).time / 1000.0)) + " " + plugin_memory.maps.get(map_idx).highscore.get(i).name);
                            }
                        }
                    }
                    return;
                }
            }
        }
    }

    @EventHandler
    public void onPlayerDisconnect(PlayerQuitEvent event)
    {
        int player_idx = plugin_memory.player_playing(event.getPlayer());
        if (player_idx != -1)
        {
            plugin_memory.current_playing_players.get(player_idx).restart = false;
            player_end(player_idx, "Disconnect!");
        }
    }

    @EventHandler
    public void onPlayerDrop(PlayerDropItemEvent event)
    {
        int player_idx = plugin_memory.player_playing(event.getPlayer());
        if (player_idx != -1) event.setCancelled(true);
    }

    static public void player_end(int idx, String message)
    {
        plugin_memory.current_playing_players.get(idx).player.setVelocity(plugin_memory.current_playing_players.get(idx).player.getVelocity().multiply(0));
        if (plugin_memory.current_playing_players.get(idx).restart)
        {
            plugin_memory.current_playing_players.get(idx).player.sendMessage(message);
            if (plugin_memory.current_playing_players.get(idx).mm.size() == 0) plugin_memory.current_playing_players.get(idx).player.sendMessage("§bRESTARTING!§r Type §a\"/elytra stop\"§r to stop restarting.");
            plugin_memory.current_playing_players.get(idx).current_checkpoint = 0;
            plugin_memory.current_playing_players.get(idx).started = false;
            plugin_memory.current_playing_players.get(idx).join_time = System.currentTimeMillis();
            plugin_memory.current_playing_players.get(idx).player.teleport(plugin_memory.maps.get(plugin_memory.current_playing_players.get(idx).map_idx).start);
        }
        else {
        Location to_port_to = plugin_memory.current_playing_players.get(idx).start_pos;
        to_port_to.setY(to_port_to.getY() + 0.1);
        plugin_memory.current_playing_players.get(idx).player.setFireTicks(0);
        plugin_memory.current_playing_players.get(idx).player.teleport(to_port_to);
        plugin_memory.current_playing_players.get(idx).player.setGameMode(plugin_memory.current_playing_players.get(idx).gm);
        if (plugin_memory.current_playing_players.get(idx).gm == GameMode.CREATIVE) plugin_memory.current_playing_players.get(idx).player.setAllowFlight(true);
        plugin_memory.current_playing_players.get(idx).player.sendMessage(message);
        plugin_memory.current_playing_players.get(idx).player.getInventory().clear();

        for (int i = 0; i < plugin_memory.current_playing_players.get(idx).player.getInventory().getSize(); i++)
        {
            plugin_memory.current_playing_players.get(idx).player.getInventory().setItem(i, plugin_memory.current_playing_players.get(idx).inv[i]);
        }
        for (int i = 0; i < 4; i++) {
            plugin_memory.current_playing_players.get(idx).player.getInventory().setItem(36+i, plugin_memory.current_playing_players.get(idx).armor[i]);
        }

        plugin_memory.current_playing_players.get(idx).player.updateInventory();
        plugin_memory.current_playing_players.remove(idx);
        }

    }
}
