package me.teknight.elytra_plugin;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by teknight on 2/26/16.
 */
public class plugin_memory {

    public static ArrayList<map> maps = new ArrayList<>();
    public static ArrayList<multimap> multimaps = new ArrayList<>();
    public static ArrayList<creating_map> map_creators = new ArrayList<>();
    public static ArrayList<playing_player> current_playing_players = new ArrayList<>();

    public enum location_types {
        BLOCK_NORMAL, BLOCK_BOOST, BLOCK_SLOW
    }

    public static int creating_map(Player player)
    {
        for (int i = 0; i < plugin_memory.map_creators.size(); i++)
        {
            if (plugin_memory.map_creators.get(i).player == player)
            {
                return i;
            }
        }
        return -1;
    }
    public static int player_playing(Player player)
    {
        for (int i = 0; i < plugin_memory.current_playing_players.size(); i++)
        {
            if (plugin_memory.current_playing_players.get(i).player == player)
            {
                return i;
            }
        }
        return -1;
    }
    public static void save_maps()
    {
        try {
            PrintWriter writer = new PrintWriter("maps.save", "UTF-8");
            for (map map_to_save : maps) {
                writer.println("<map");
                writer.println("name: " + map_to_save.map_name);
                writer.println("start," + map_to_save.start.getWorld().getName() + "," + map_to_save.start.getBlockX() + "," + map_to_save.start.getBlockY() + "," + map_to_save.start.getBlockZ());
                writer.println("<highscore");
                for (highscores score : map_to_save.highscore) {
                    writer.println("score: " + score.time + " " + score.name);
                }
                writer.println(">highscore");
                writer.println("<checkpoints");
                for (ArrayList<loc_points> points : map_to_save.checkpoints)
                {
                    writer.println("<points");
                    for (loc_points point : points)
                    {
                        String point_name = "point";
                        if (point.loc_type == location_types.BLOCK_BOOST) point_name += "b";
                        else if (point.loc_type == location_types.BLOCK_SLOW) point_name += "s";
                        writer.println(point_name + "," + point.loc.getWorld().getName() + "," + point.loc.getBlockX() + "," + point.loc.getBlockY() + "," + point.loc.getBlockZ());
                    }
                    writer.println(">points");
                }
                writer.println(">checkpoints");
                writer.println(">map");
            }
            for (multimap mmmap_to_save : multimaps) {
                writer.println("<multimap");
                writer.println("mmname: " + mmmap_to_save.multimap_name);
                writer.print("mmmaps: ");
                for (int mapidx: mmmap_to_save.map_idxs) {
                    writer.print(mapidx + " ");
                }
                writer.print("\n");
                writer.println("<mmhighscore");
                for (highscores score : mmmap_to_save.highscore) {
                    writer.println("mmscore: " + score.time + " " + score.name);
                }
                writer.println(">mmhighscore");
                writer.println(">multimap");
            }
            writer.close();
        } catch (Exception e)
        {
            System.out.print(e);
        }
    }
    public static void load_maps() {
        try {
            maps.clear();
            BufferedReader br = new BufferedReader(new FileReader("maps.save"));
            String line = br.readLine();

            map map_to_add = new map("temp_name");
            multimap mm_map_to_add = new multimap("mm_temp_name");

            while (line != null) {
                if (line.equals("<map"))
                {
                    map_to_add.checkpoints.clear();
                    map_to_add.new_checkpoint.clear();
                }
                if (line.startsWith("name:")) map_to_add.map_name = line.split(" ")[1];
                if (line.startsWith("mmname:")) mm_map_to_add.multimap_name = line.split(" ")[1];
                if (line.startsWith("mmmaps:"))
                {
                    String[] map_indexes = line.split(" ");
                    for (int i = 1; i < map_indexes.length; i++) {
                        mm_map_to_add.map_idxs.add(Integer.parseInt(map_indexes[i]));
                    }
                }
                if (line.startsWith("start,"))
                {
                    String[] cords = line.split(",");
                    map_to_add.start = new Location(Bukkit.getWorld(cords[1]), Double.parseDouble(cords[2]), Double.parseDouble(cords[3]), Double.parseDouble(cords[4]));
                }
                if (line.equals("<checkpoints")) map_to_add.checkpoints.clear();
                if (line.equals("<highscore")) map_to_add.highscore.clear();
                if (line.startsWith("score:")) map_to_add.highscore.add(new highscores(Long.parseLong(line.split(" ")[1]), line.split(" ")[2]));
                if (line.equals("<mmhighscore")) mm_map_to_add.highscore.clear();
                if (line.startsWith("mmscore:")) mm_map_to_add.highscore.add(new highscores(Long.parseLong(line.split(" ")[1]), line.split(" ")[2]));
                if (line.equals("<points")) map_to_add.checkpoints.add(new ArrayList<loc_points>());
                if (line.startsWith("point,"))
                {
                    String[] cords = line.split(",");
                    map_to_add.checkpoints.get(map_to_add.checkpoints.size()-1).add(new loc_points(new Location(Bukkit.getWorld(cords[1]), Double.parseDouble(cords[2]), Double.parseDouble(cords[3]), Double.parseDouble(cords[4])), location_types.BLOCK_NORMAL));
                }
                if (line.startsWith("pointb,"))
                {
                    String[] cords = line.split(",");
                    map_to_add.checkpoints.get(map_to_add.checkpoints.size()-1).add(new loc_points(new Location(Bukkit.getWorld(cords[1]), Double.parseDouble(cords[2]), Double.parseDouble(cords[3]), Double.parseDouble(cords[4])), location_types.BLOCK_BOOST));
                }
                if (line.startsWith("points,"))
                {
                    String[] cords = line.split(",");
                    map_to_add.checkpoints.get(map_to_add.checkpoints.size()-1).add(new loc_points(new Location(Bukkit.getWorld(cords[1]), Double.parseDouble(cords[2]), Double.parseDouble(cords[3]), Double.parseDouble(cords[4])), location_types.BLOCK_SLOW));
                }
                if (line.equals(">map")){
                    map _map = new map(map_to_add.map_name);
                    _map.start = map_to_add.start;
                    for (ArrayList<loc_points> points : map_to_add.checkpoints)
                    {
                        _map.checkpoints.add(points);
                    }
                    for (highscores score : map_to_add.highscore)
                    {
                        _map.highscore.add(score);
                    }
                    maps.add(_map);
                }
                if (line.equals(">multimap")){
                    multimap _mmmap = new multimap(mm_map_to_add.multimap_name);
                    for (int mapidx : mm_map_to_add.map_idxs)
                    {
                        _mmmap.map_idxs.add(mapidx);
                    }
                    for (highscores score : mm_map_to_add.highscore)
                    {
                        _mmmap.highscore.add(score);
                    }
                    multimaps.add(_mmmap);
                }

                line = br.readLine();
            }
            Bukkit.broadcastMessage("Maps loaded!");
        } catch (Exception e)
        {
            System.out.print(e);
        }
    }


    public static void add_to_highscore(int map_idx, long time, String player_name)
    {
        highscores score = new highscores();
        score.time = time;
        score.name = player_name;
        if (maps.get(map_idx).highscore.size() == 0)
        {
            maps.get(map_idx).highscore.add(score);
        }
        else
        {
            boolean added = false;
            for (int i = 0; i < maps.get(map_idx).highscore.size(); i++)
            {
                if (maps.get(map_idx).highscore.get(i).time > time)
                {
                    maps.get(map_idx).highscore.add(i, score);
                    added = true;
                    break;
                }
            }
            if (!added && maps.get(map_idx).highscore.size() < 3) maps.get(map_idx).highscore.add(score);
            if (maps.get(map_idx).highscore.size() > 3) maps.get(map_idx).highscore.remove(3);
        }
        save_maps();
    }
    public static void add_to_mm_highscore(int mm_map_idx, long time, String player_name)
    {
        highscores score = new highscores();
        score.time = time;
        score.name = player_name;
        if (multimaps.get(mm_map_idx).highscore.size() == 0)
        {
            multimaps.get(mm_map_idx).highscore.add(score);
        }
        else
        {
            boolean added = false;
            for (int i = 0; i < multimaps.get(mm_map_idx).highscore.size(); i++)
            {
                if (multimaps.get(mm_map_idx).highscore.get(i).time > time)
                {
                    multimaps.get(mm_map_idx).highscore.add(i, score);
                    added = true;
                    break;
                }
            }
            if (!added && multimaps.get(mm_map_idx).highscore.size() < 3) multimaps.get(mm_map_idx).highscore.add(score);
            if (multimaps.get(mm_map_idx).highscore.size() > 3) multimaps.get(mm_map_idx).highscore.remove(3);
        }
        save_maps();
    }
}

class highscores
{
    long time;
    String name;
    highscores()
    {

    }
    highscores(long _time, String _name)
    {
        time = _time;
        name = _name;
    }
}

class creating_map
{
    Player player;
    int map_idx;
    creating_map(Player _player, int _map_idx)
    {
        player = _player;
        map_idx = _map_idx;
    }
}

class loc_points
{
    Location loc;
    plugin_memory.location_types loc_type;
    loc_points(Location _loc, plugin_memory.location_types _loc_type)
    {
        loc = _loc;
        loc_type = _loc_type;
    }
    static plugin_memory.location_types get_loc_type(String loc_type_to_get)
    {
        switch (loc_type_to_get)
        {
            case "S":
                return plugin_memory.location_types.BLOCK_SLOW;
            case "B":
                return plugin_memory.location_types.BLOCK_BOOST;
            default:
                return plugin_memory.location_types.BLOCK_NORMAL;
        }
    }
}

class multimap
{
    String multimap_name;
    ArrayList<Integer> map_idxs = new ArrayList<>();
    public ArrayList<highscores> highscore = new ArrayList<>();
    multimap(String _map_name)
    {
        multimap_name = _map_name;
    }
}

class map
{
    public String map_name;
    public ArrayList<ArrayList<loc_points>> checkpoints = new ArrayList<>();
    public ArrayList<loc_points> new_checkpoint = new ArrayList<>();
    public Location start;
    public ArrayList<highscores> highscore = new ArrayList<>();
    map(String _map_name)
    {
        map_name = _map_name;
    }
}

class playing_player
{
    Player player;
    int map_idx;
    ItemStack[] inv;
    ItemStack[] armor;
    int current_checkpoint;
    long start_time;
    long join_time;
    boolean started;
    boolean restart;
    ArrayList<Integer> mm = new ArrayList<>();
    int mm_idx;
    boolean mm_started;
    GameMode gm;
    Location start_pos;
    playing_player(Player _player, int _map_idx, Location _start_pos)
    {
        player = _player;
        map_idx = _map_idx;
        start_pos = _start_pos;
        current_checkpoint = 0;
    }
}