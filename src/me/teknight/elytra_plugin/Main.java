package me.teknight.elytra_plugin;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.plugin.java.JavaPlugin;



/**
 * Created by teknight on 2/26/16.
 */
public class Main extends JavaPlugin {


    @Override
    public void onEnable() {

        this.getCommand("elytra").setExecutor(new elytra_cmd());
        getServer().getPluginManager().registerEvents(new elytra_builder(), this);
        getServer().getPluginManager().registerEvents(new elytra_movement(), this);

        plugin_memory.load_maps();

        Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(this, new  Runnable(){
            public void run(){
                if (plugin_memory.current_playing_players.size() != 0)
                {
                    for (int i = 0; i < plugin_memory.current_playing_players.size(); i++)
                    {
                        if ((plugin_memory.current_playing_players.get(i).player.isOnGround() || plugin_memory.current_playing_players.get(i).player.getInventory().getChestplate().getType() != Material.ELYTRA) && plugin_memory.current_playing_players.get(i).started) elytra_movement.player_end(i, elytra_movement.lose_message);
                    }
                }
            }}, 1l, 1l);
    }

    @Override
    public void onDisable() {

    }
}
