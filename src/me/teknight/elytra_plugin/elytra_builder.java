package me.teknight.elytra_plugin;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

import java.util.ArrayList;


/**
 * Created by teknight on 2/26/16.
 */
public class elytra_builder implements Listener {

    @EventHandler
    public void onPlayerBreakBlock(BlockBreakEvent event)
    {
        if (plugin_memory.player_playing(event.getPlayer()) != -1) event.setCancelled(true);
        if (event.getPlayer().getItemInHand().getType() == Material.STONE_HOE)
        {
            int map_creation_idx;
            try {map_creation_idx = plugin_memory.map_creators.get(plugin_memory.creating_map(event.getPlayer())).map_idx;}
            catch (Exception e)
            {
                return;
            }
            if (map_creation_idx == -1) return;
            if (event.getBlock().getType() == Material.WOOL) {
                if (map_creation_idx != -1) {

                    break_all_surroundig_blocks(event.getBlock(), map_creation_idx);
                    plugin_memory.maps.get(map_creation_idx).checkpoints.add(new ArrayList<loc_points>(plugin_memory.maps.get(map_creation_idx).new_checkpoint));
                    plugin_memory.maps.get(map_creation_idx).new_checkpoint.clear();
                    event.getPlayer().sendMessage("Current checkpoints: §b" + plugin_memory.maps.get(map_creation_idx).checkpoints.size() + "§r");
                }
            } else if (event.getBlock().getType() == Material.DIRT)
            {
                plugin_memory.maps.get(map_creation_idx).start = event.getBlock().getLocation();
                plugin_memory.maps.get(map_creation_idx).start.setY(plugin_memory.maps.get(map_creation_idx).start.getY() + 1);
                event.getBlock().setType(Material.AIR);
                event.getPlayer().sendMessage("Start location placed!");
            }
        }
    }
    @EventHandler
    public void onPlayerPlaceBlock(BlockPlaceEvent event)
    {
        if (plugin_memory.player_playing(event.getPlayer()) != -1) event.setCancelled(true);
    }

    public void break_all_surroundig_blocks(Block block, int map_idx)
    {
        switch ((short)block.getData())
        {
            case 0:
                plugin_memory.maps.get(map_idx).new_checkpoint.add(new loc_points(block.getLocation(), plugin_memory.location_types.BLOCK_NORMAL));
                break;
            case 11:
                plugin_memory.maps.get(map_idx).new_checkpoint.add(new loc_points(block.getLocation(), plugin_memory.location_types.BLOCK_SLOW));
                break;
            case 14:
                plugin_memory.maps.get(map_idx).new_checkpoint.add(new loc_points(block.getLocation(), plugin_memory.location_types.BLOCK_BOOST));
                break;
            default:
                return;
        }

        block.setType(Material.AIR);

        if (block.getRelative(1,  0, 0).getType() == Material.WOOL) break_all_surroundig_blocks(block.getRelative(1,  0, 0), map_idx);
        if (block.getRelative(-1, 0, 0).getType() == Material.WOOL) break_all_surroundig_blocks(block.getRelative(-1, 0, 0), map_idx);
        if (block.getRelative(0,  1, 0).getType() == Material.WOOL) break_all_surroundig_blocks(block.getRelative(0,  1, 0), map_idx);
        if (block.getRelative(0, -1, 0).getType() == Material.WOOL) break_all_surroundig_blocks(block.getRelative(0, -1, 0), map_idx);
        if (block.getRelative(0,  0, 1).getType() == Material.WOOL) break_all_surroundig_blocks(block.getRelative(0,  0, 1), map_idx);
        if (block.getRelative(0, 0, -1).getType() == Material.WOOL) break_all_surroundig_blocks(block.getRelative(0, 0, -1), map_idx);
    }
}
